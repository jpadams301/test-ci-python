import anyio
import dagger
import sys

async def main():
    async with dagger.Connection(dagger.Config(log_output=sys.stderr)) as client:
        ctr = (
            client.container()
            .from_("alpine")
            .with_exec(["apk", "add", "curl"])
            .with_exec(["curl", "https://lite.pbs.org/"])
        )
        output = await ctr.stdout()
        print(output[:300])

anyio.run(main)
